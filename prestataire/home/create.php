<?php include 'header.php';
require_once '../../controller/ServiceController.php';?>
<link rel="stylesheet" href="../../assets/alert/alertify.min.css">
<script src="../../assets/alert/alertify.min.js"></script>

<div class="main-panel">
    <div class="content-wrapper">
        <form method="post" action="create.php" enctype="multipart/form-data">
            <div class="row">
                <div class="col-md-4">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <?php if(count($err)>0):?>
                            <div class="alert alert-danger">
                                <?php foreach($err as $e):?>
                                <li><?php echo $e;?></li>
                                <?php endforeach;?>
                            </div>
                            <?php  endif;?>
                            <select class="form-control" <?php echo $secteur;?> required name="secteur" id="categorie">
                                <option disabled="" selected="" value="">Selectionner le secteur d'activité</option>
                                <?php

                    $sql = "SELECT nom_categorie , id FROM categorie";
                    $result = $conn->query($sql);
                    if ($result->rowCount() > 0) {
                      while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                        $categorie=$row['nom_categorie'];$id =$row['id'];
?>

                                <option style="font-size: 12px;" value="<?php echo $id?>">
                                    <?php echo strtoupper($categorie); ?></option>
                                <?php
                      }

                    }
                  ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <select class="form-control" id="metier" <?php echo $domaine;?> required name="domaine">
                                <option value="" selected="" disabled="">Selectionner votre profession</option>

                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group row">
                            <select class="form-control" <?php echo $aexperience;?> name="aexperience" required>
                                <option value="" selected="" disabled="">Nombre d'année d'expérience</option>
                                <option>0</option>
                                <option>1</option>
                                <option>2</option>
                                <option>+ de 3</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputUsername1">Justificatif profession</label>
                            <input type="file" <?php echo $fileName;?> class="form-control" id="exampleInputUsername1"
                                required name="jprofession" placeholder="Username">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputUsername1">Images réalisation </label>
                            <input type="file" class="form-control" id="exampleInputUsername1" required
                                name='realisation[]' multiple>
                        </div>
                    </div>

                </div>

                <div class="col-md-8">
                    <label for="description">Description</label>
                    <textarea id="editor" name="edit">
                </textarea>
                    <br>
                    <br>
                    <button type="submit" name="service" class="btn btn-info mr-2" id="soumettre"
                        style="float:right">Soumettre</button>
                </div>
            </div>
        </form>
    </div>
</div>


</div>
</div>


<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
<script>
CKEDITOR.replace('editor');
</script>
<script>
document.getElementById("categorie").addEventListener("change", function(e) {
    var categorie = document.getElementById("categorie").value;
    $.ajax({
        url: 'service.php',
        type: 'GET',
        data: 'idCat=' + categorie,
        dataType: 'json',
        success: function(data, i) {
            $("#metier").empty();
            for (i = 0; i < data.length; i++) {
                $("#metier").append(
                    "<option value=" + data[i]['id'] + ">" +
                    data[i]['nom_secteur'] +
                    "</option>"
                );
            }
        },




    });
})
</script>

<!-- container-scroller -->

<!-- base:js -->
<script src="vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="js/off-canvas.js"></script>
<script src="js/hoverable-collapse.js"></script>
<script src="js/template.js"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="js/file-upload.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="js/dashboard.js"></script>
<script>
function notification() {
    window.location.assign('notification.php')
    return false;
}
</script>
</body>

</html>