<?php include 'header.php';?>

<br>
<div class="main-panel">
    <div class="content-wrapper">


        <div class="row" style="padding: 25px;">
            <div class="table-responsive">
                <table id="example" class="table table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th style="font-weight: bold;">N <sup>0</sup></th>
                            <th style="font-weight: bold;">Secteur</th>
                            <th style="font-weight: bold;">Profession</th>
                            <th style="font-weight: bold;">Creation</th>
                            <th style="font-weight: bold;">Statut</th>
                            <th style="font-weight: bold;">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                      $sql = "SELECT * FROM services WHERE fk_p_s='$id' ORDER BY id DESC";
                      $result = $conn->query($sql);
                      if ($result->rowCount() > 0) {
                        $i=1;
                        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                          $id=$row["id"];
                          $prestataire=$row["fk_p_s"];
                          $img_just=$row["file_just"];
                          $categorie=$row["categorie"];
                          $metier=$row["metier"];
                          $description=$row["description"];
                          $creation=$row["date_creation"];
                          $status=$row['status'];
                          ?>
                        <tr id="elt<?php echo $id;?>">
                            <td><?php echo $i++ ?></td>

                            <td><?php 
                          
                          $stmt = "SELECT nom_categorie FROM categorie WHERE id='$categorie' LIMIT 1"; 
                          $result1=$conn->query($stmt);
                          $row = $result1->fetch(PDO::FETCH_ASSOC);
                          echo $row['nom_categorie'];
                          
                          ?></td>
                            <td><?php
                          
                          $stmt = "SELECT nom_secteur FROM secteur WHERE id='$metier' LIMIT 1"; 
                          $result1=$conn->query($stmt);
                          $row = $result1->fetch(PDO::FETCH_ASSOC);
                          echo $row['nom_secteur'];

                          ?></td>
                            <td><?php echo $creation; ?></td>
                            <td>
                                <?php
                                    if($status==0){?>
                                <button type="button" class="btn btn-warning btn-icon-text">
                                    <i class="mdi mdi-loading btn-icon-prepend"></i>
                                    En attente
                                </button><?php
                                        
                                    }elseif ($status==1) {?>

                                <button type="button" class="btn btn-info btn-icon-text">
                                    <i class="mdi mdi-check btn-icon-append"></i>

                                    Validé
                                </button>
                                <?php

                                    

                                }else{
                                ?>
                                <button type="button" class="btn btn-danger btn-icon-text">
                                    <i class="mdi mdi-upload btn-icon-prepend"></i>
                                    Rejeté
                                </button>
                                <?php
                                }
                                ?>


                            </td>
                            <td>
                                <button type="button" class="btn btn-social-icon btn-outline-linkedin"><i
                                        class="mdi mdi-pencil"></i></button>
                                <button onclick="supprimer(<?php echo $id;?>)" type="button"
                                    class="btn btn-social-icon btn-outline-google"><i
                                        class="mdi mdi-delete"></i></button>






                            </td>
                        </tr>
                        <?php
                        }
                      }
                      else {
                      }
                  ?>

                </table>
            </div>
        </div>
    </div>
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<script>
function supprimer(id) {
    $.ajax({
        url: 'del.php', // La ressource ciblée
        type: 'GET', // Le type de la requête HTTP.
        data: {
            id: id
        },
        success: function(response) {
            if (response == 1) {
                $('#elt' + id).hide()
            }
        }
    });
}
</script>
<!-- base:js -->
<script src="vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="js/off-canvas.js"></script>
<script src="js/hoverable-collapse.js"></script>
<script src="js/template.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="js/dashboard.js"></script>
<!-- End custom js for this page-->
</body>

</html>