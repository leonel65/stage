<?php include 'header.php';?>
<!-- partial -->
<div class="main-panel">
    <div class="content-wrapper">
        <div class="container" style="padding-top: 60px;">
            <div class="row">
                <!-- left column -->
                <div class="col-md-4 col-sm-6 col-xs-12">

                    <div class="text-center">
                        <img src="../../assets/img/profil/3.png" class="avatar img-circle img-thumbnail" alt="avatar">
                        <h6></h6>
                        <input type="file" class="text-center center-block well well-sm">
                    </div>
                </div>
                <!-- edit form column -->
                <div class="col-md-8 col-sm-6 col-xs-12 personal-info">

                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Pseudo</label>
                            <div class="col-lg-8">
                                <input class="form-control" value="Leonel65" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Nom</label>
                            <div class="col-lg-8">
                                <input class="form-control" value="Onsebat" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Prenom</label>
                            <div class="col-lg-8">
                                <input class="form-control" value="Onsebat" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Email:</label>
                            <div class="col-lg-8">
                                <input class="form-control" value="serviceonsebat@gmail.com" type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3 control-label">Ville</label>
                            <div class="col-lg-8">
                                <div class="ui-select">
                                    <select id="user_time_zone" class="form-control">
                                        <option value="Hawaii">(GMT-10:00) Hawaii</option>
                                        <option value="Alaska">(GMT-09:00) Alaska</option>
                                        <option value="Pacific Time (US & Canada)">(GMT-08:00) Pacific Time (US &
                                            Canada)</option>
                                        <option value="Arizona">(GMT-07:00) Arizona</option>
                                        <option value="Mountain Time (US & Canada)">(GMT-07:00) Mountain Time (US &
                                            Canada)</option>
                                        <option value="Central Time (US & Canada)" selected="selected">(GMT-06:00)
                                            Central Time (US & Canada)</option>
                                        <option value="Eastern Time (US & Canada)">(GMT-05:00) Eastern Time (US &
                                            Canada)</option>
                                        <option value="Indiana (East)">(GMT-05:00) Indiana (East)</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label"></label>
                            <div class="col-md-8">
                                <input class="btn btn-primary" value="Save Changes" type="button">
                                <span></span>
                                <input class="btn btn-default" value="Cancel" type="reset">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- container-scroller -->

<!-- base:js -->
<script src="vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="js/off-canvas.js"></script>
<script src="js/hoverable-collapse.js"></script>
<script src="js/template.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="js/dashboard.js"></script>
<!-- End custom js for this page-->
</body>

</html>