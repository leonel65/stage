<?php
         
            require_once 'controller/authController.php';
            $conn = new PDO("mysql:host=localhost;dbname=stage", 'root', '');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Onsebat">
    <meta name="keywords" content="Onsebat">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" type="image/png" href="assets/img/first/o.png" />

    <title>Onsebat</title>
    <link rel="stylesheet" href="prestataire/home/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="assets/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="assets/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&display=swap" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Texturina:wght@300&display=swap" rel="stylesheet">
    <style>
    body {
        font-family: 'Lato', sans-serif;
    }
    </style>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/60c3fec87f4b000ac0372ea1/1f7upmg5g';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
    </script>
    <!--End of Tawk.to Script-->
</head>

<body>
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="#"><img src="assets/img/logo.png" alt=""></a>
        </div>
        <div class="humberger__menu__widget">
            <div class="header__top__right__language">
                <img src="assets/img/first/france.png" height="27px" width="27px" alt="">
                <div>Francais</div>
                <span class="arrow_carrot-down"></span>
                <ul>
                    <li><a href="#">Francais</a></li>
                    <li><a href="#">English</a></li>
                </ul>
            </div>

        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="views/shop-grid.html">a propos</a></li>
                <li><a href="views/contact.html">Contact</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
        </div>
        <div class="humberger__menu__contact">
            <ul>
                <li><i class="fa fa-envelope"></i> infos@onsebat.com</li>
            </ul>
        </div>
    </div>
    <!-- Humberger End -->

    <!-- Header Section Begin -->
    <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4">
                        <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> infos@onsebat.com</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8">
                        <div class="header__top__right">
                            <div class="header__top__right__social">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                            <div class="header__top__right__language">
                                <img src="assets/img/first/france.png" height="27px" width="27px" alt="">
                                <div>Francais</div>
                                <span class="arrow_carrot-down"></span>
                                <ul>
                                    <li><a href="#">Francais</a></li>
                                    <li><a href="#">English</a></li>
                                </ul>
                            </div>
                            <?php if(isset($_SESSION['connect'])): ?>



                            <div class="header__top__right__language">
                                <img src="assets/img/profil/user.png" height="20px" width="20px" alt="">
                                <div><?php echo $_SESSION['lastname'].' '.$_SESSION['firstname']; ?></div>
                                <span class="arrow_carrot-down"></span>
                                <ul>
                                    <li><a href="">Profil</a></li>
                                    <li><a href="logout.php">Deconnexion</a></li>
                                </ul>
                            </div>


                            <?php else: ?>
                            <div class="header__top__right__language">
                                <div class="header__top__right__auth">
                                    <a href="prestataire/index.php?token&err=0"><i
                                            class="mdi mdi mdi-worker menu-icon"></i> Prestataire</a>
                                </div>
                            </div>
                            <div class="header__top__right__language">
                                <div class="header__top__right__auth">
                                    <a href="login.php?token&err=0"><i class="mdi mdi mdi-account menu-icon"></i>
                                        Utilisateur</a>
                                </div>
                            </div>
                            <?php endif ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="index.php"><img src="assets/img/first/logo.png" alt=""></a>
                    </div>
                </div>

            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->
    <!-- Hero Section Begin -->
    <section class="hero hero-normal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Nos secteurs</span>
                        </div>
                        <ul>
                            <?php
                    $sql = "SELECT * FROM categorie WHERE statut=0";
                    $result = $conn->query($sql);
                    if ($result->rowCount() > 0) {
                      while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                        $id=$row["id"];
                        $nom=$row["nom_categorie"];
                        ?>
                            <li><a href="redirection.php?id=<?php echo $id;?>"
                                    style="font-size: 12px;"><?php echo strtoupper($nom);?></a></li>
                            <?php
                      }
                    }
                    else {
                    }
                ?>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="index.php" method="post">
                                <input type="text" placeholder="Exple: peintre, memuisier " name="search">
                                <button type="submit" name="recherche" class="site-btn">chercher</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>+237 6 20 03 75 43</h5>
                                <span>disponible 24/24 - 7j/7</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>