<?php
session_start();
      if ($_SESSION['connect']) {
        $conn = new PDO("mysql:host=localhost;dbname=stage", 'root', '');
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      }else{
        header("location: ../index.php");
      }


?>

<html lang="en">

<head>
    <title>Dashboard | Utilisateur</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- VENDOR CSS -->

    <link rel="stylesheet" href="assets/vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/vendor/linearicons/style.css">
    <link rel="stylesheet" href="assets/vendor/chartist/css/chartist-custom.css">
    <!-- MAIN CSS -->
    <link rel="stylesheet" href="assets/css/main.css">
    <!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
    <link rel="stylesheet" href="assets/css/demo.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.11.1/css/dataTables.bootstrap4.min.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&display=swap" rel="stylesheet">
    <!-- ICONS -->
    <link rel="icon" type="image/png <link rel=" icon" type="image/png" href="../assets/img/first/o.png" />"
    sizes="96x96"
    href="assets/img/favicon.png">
</head>
<style media="screen">
  body{
    font-family: lato
  }
</style>
<body>
    <!-- WRAPPER -->
    <div id="wrapper">
        <!-- NAVBAR -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="brand">
                <a href="index.php"><img src="../assets/img/first/logo.png" style="height:45px" alt="Klorofil Logo"
                        class="img-responsive logo"></a>
            </div>
            <div class="container-fluid">
                <div class="navbar-btn">
                    <button type="button" class="btn-toggle-fullwidth"><i
                            class="lnr lnr-arrow-left-circle"></i></button>
                </div>
                <form class="navbar-form navbar-left">
                    <div class="input-group">
                        <input type="text" value="" style="height:45px" class="form-control" placeholder="Recherche">
                        <span class="input-group-btn"><button type="button"
                                class="btn btn-primary btn-md" style="height:45px;border:green">Rechercher</button></span>
                    </div>
                </form>

                <div id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="">
                            <a href="#" class="dropdown-toggle nbrec" data-toggle="dropdown"><i
                                    class="fa fa-bell"></i> <sup class="nbrc" style="color:green;font-weight:bold;font-size:18px">
                                    </sup>  Non Lu(s)</a>


                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="assets/img/user.png"
                                    class="img-circle" alt="Avatar"> <span><?php
                                    $id=$_SESSION['id'];
                                   $stmt = "SELECT firstname,lastname FROM users WHERE id='$id' LIMIT 1";
                                  $result1=$conn->query($stmt);
                                  $row = $result1->fetch(PDO::FETCH_ASSOC);
                                  echo $row['lastname'].' ' .$row['firstname'];


                                  ?></span> <i
                                    class="icon-submenu lnr lnr-chevron-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"><i class="lnr lnr-user"></i> <span>Mon profil</span></a></li>
                                <li><a href="../logout.php"><i class="lnr lnr-exit"></i> <span>Deconnexion</span></a>
                                </li>
                            </ul>
                        </li>
                        <!-- <li>
							<a class="update-pro" href="https://www.themeineed.com/downloads/klorofil-pro-bootstrap-admin-dashboard-template/?utm_source=klorofil&utm_medium=template&utm_campaign=KlorofilPro" title="Upgrade to Pro" target="_blank"><i class="fa fa-rocket"></i> <span>UPGRADE TO PRO</span></a>
						</li> -->
                    </ul>
                </div>
            </div>
        </nav>
