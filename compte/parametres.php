<?php include 'header.php';?>
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="index.php" class=""><i class="lnr lnr-home"></i> <span>Dashboard</span></a>
                </li>
                <li><a href="chat.php" class=""><i class="lnr lnr-envelope"></i> <span>Messages</span></a>
                </li>

                <li><a href="parametres.php" class="active"><i class="lnr lnr-cog"></i> <span>Paramètres</span></a></li>
            </ul>
        </nav>
    </div>
</div>
<!-- END LEFT SIDEBAR -->
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
        <div class="container-fluid">
            <h3 class="page-title">Panels</h3>
            <div class="row">
                <div class="col-md-8">
                    <!-- PANEL HEADLINE -->
                    <div class="panel panel-headline">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel Headline</h3>
                            <p class="panel-subtitle">Panel to display most important information</p>
                        </div>
                        <div class="panel-body">
                            <h4>Panel Content</h4>
                            <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently
                                initiate go forward leadership skills before an expanded array of infomediaries.
                                Monotonectally incubate web-enabled communities rather than process-centric.</p>
                        </div>
                    </div>
                    <!-- END PANEL HEADLINE -->
                </div>
                <div class="col-md-4">
                    <!-- PANEL NO PADDING -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel No Padding</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse"><i
                                        class="lnr lnr-chevron-up"></i></button>
                                <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                            </div>
                        </div>
                        <div class="panel-body no-padding bg-primary text-center">
                            <div class="padding-top-30 padding-bottom-30">
                                <i class="fa fa-thumbs-o-up fa-5x"></i>
                                <h3>No Content Padding</h3>
                            </div>
                        </div>
                    </div>
                    <!-- END PANEL NO PADDING -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <!-- PANEL DEFAULT -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel Default</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse"><i
                                        class="lnr lnr-chevron-up"></i></button>
                                <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently
                                initiate go forward leadership skills before an expanded array of infomediaries.
                                Monotonectally incubate web-enabled communities rather than process-centric.</p>
                        </div>
                    </div>
                    <!-- END PANEL DEFAULT -->
                </div>
                <div class="col-md-4">
                    <!-- PANEL NO CONTROLS -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel No Right Controls</h3>
                        </div>
                        <div class="panel-body">
                            <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently
                                initiate go forward leadership skills before an expanded array of infomediaries.
                                Monotonectally incubate web-enabled communities rather than process-centric.</p>
                        </div>
                    </div>
                    <!-- END PANEL NO CONTROLS -->
                </div>
                <div class="col-md-4">
                    <!-- PANEL WITH FOOTER -->
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel With Footer</h3>
                            <div class="right">
                                <button type="button" class="btn-toggle-collapse"><i
                                        class="lnr lnr-chevron-up"></i></button>
                                <button type="button" class="btn-remove"><i class="lnr lnr-cross"></i></button>
                            </div>
                        </div>
                        <div class="panel-body">
                            <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently
                                initiate go forward leadership skills before an expanded array.</p>
                        </div>
                        <div class="panel-footer">
                            <h5>Panel Footer</h5>
                        </div>
                    </div>
                    <!-- END PANEL WITH FOOTER -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <!-- PANEL SCROLLING -->
                    <div id="panel-scrolling-demo" class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Panel Scrolling</h3>
                        </div>
                        <div class="panel-body">
                            <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently
                                initiate go forward leadership skills before an expanded array of infomediaries.
                                Monotonectally incubate web-enabled communities rather than process-centric.</p>
                            <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently
                                initiate go forward leadership skills before an expanded array of infomediaries.
                                Monotonectally incubate web-enabled communities rather than process-centric.</p>
                            <p>Objectively network visionary methodologies via best-of-breed users. Phosfluorescently
                                initiate go forward leadership skills before an expanded array of infomediaries.
                                Monotonectally incubate web-enabled communities rather than process-centric.</p>
                        </div>
                    </div>
                    <!-- END PANEL SCROLLING -->
                </div>
            </div>
        </div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<div class="clearfix"></div>
<footer>
    <div class="container-fluid">
        <p class="copyright">Shared by <i class="fa fa-love"></i><a
                href="https://bootstrapthemes.co">BootstrapThemes</a>
        </p>
    </div>
</footer>
</div>
<script type="text/javascript">
setInterval(function(){ $.ajax({
    url: 'refrech.php',
    type: 'GET',
    data: 'idu=' + <?php echo $_SESSION['id'];?>,
    dataType: 'json',
    success: function(data) {

            $(".nbrc").html(
                '<sub>'+data[0]['nb']+'<sub>'

            );

    },




});}, 1);
</script>
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
