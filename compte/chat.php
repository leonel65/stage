<?php include 'header.php'; ?>
<style media="screen">
body{
  background: #F4F7FD;
  margin-top:20px;
}

.card-margin {
  margin-bottom: 1.875rem;
}

.card {
  border: 0;
  box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
  -webkit-box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
  -moz-box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
  -ms-box-shadow: 0px 0px 10px 0px rgba(82, 63, 105, 0.1);
}
.card {
  position: relative;
  display: flex;
  flex-direction: column;
  min-width: 0;
  word-wrap: break-word;
  background-color: #ffffff;
  background-clip: border-box;
  border: 1px solid #e6e4e9;
  border-radius: 8px;
}

.card .card-header.no-border {
  border: 0;
}
.card .card-header {
  background: none;
  padding: 0 0.9375rem;
  font-weight: 500;
  display: flex;
  align-items: center;
  min-height: 50px;
}
.card-header:first-child {
  border-radius: calc(8px - 1px) calc(8px - 1px) 0 0;
}

.widget-49 .widget-49-title-wrapper {
display: flex;
align-items: center;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-primary {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #edf1fc;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-primary .widget-49-date-day {
color: #4e73e5;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-primary .widget-49-date-month {
color: #4e73e5;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-secondary {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #fcfcfd;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-secondary .widget-49-date-day {
color: #dde1e9;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-secondary .widget-49-date-month {
color: #dde1e9;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-success {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #e8faf8;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-success .widget-49-date-day {
color: #17d1bd;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-success .widget-49-date-month {
color: #17d1bd;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-info {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #ebf7ff;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-info .widget-49-date-day {
color: #36afff;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-info .widget-49-date-month {
color: #36afff;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-warning {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: floralwhite;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-warning .widget-49-date-day {
color: #FFC868;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-warning .widget-49-date-month {
color: #FFC868;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-danger {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #feeeef;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-danger .widget-49-date-day {
color: #F95062;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-danger .widget-49-date-month {
color: #F95062;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-light {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #fefeff;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-light .widget-49-date-day {
color: #f7f9fa;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-light .widget-49-date-month {
color: #f7f9fa;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-dark {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #ebedee;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-dark .widget-49-date-day {
color: #394856;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-dark .widget-49-date-month {
color: #394856;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-base {
display: flex;
align-items: center;
justify-content: center;
flex-direction: column;
background-color: #f0fafb;
width: 4rem;
height: 4rem;
border-radius: 50%;
}

.widget-49 .widget-49-title-wrapper .widget-49-date-base .widget-49-date-day {
color: #68CBD7;
font-weight: 500;
font-size: 1.5rem;
line-height: 1;
}
ul.b {
  list-style-type: square;
}
.widget-49 .widget-49-title-wrapper .widget-49-date-base .widget-49-date-month {
color: #68CBD7;
line-height: 1;
font-size: 1rem;
text-transform: uppercase;
}

.widget-49 .widget-49-title-wrapper .widget-49-meeting-info {
display: flex;
flex-direction: column;
margin-left: 1rem;
}

.widget-49 .widget-49-title-wrapper .widget-49-meeting-info .widget-49-pro-title {
color: #3c4142;
font-size: 14px;
}

.widget-49 .widget-49-title-wrapper .widget-49-meeting-info .widget-49-meeting-time {
color: #B1BAC5;
font-size: 13px;
}

.widget-49 .widget-49-meeting-points {
font-weight: 400;
font-size: 13px;
margin-top: .5rem;
}

.widget-49 .widget-49-meeting-points .widget-49-meeting-item {
display: list-item;
color: #727686;
}

.widget-49 .widget-49-meeting-points .widget-49-meeting-item span {
margin-left: .5rem;
}

.widget-49 .widget-49-meeting-action {
text-align: right;
}

.widget-49 .widget-49-meeting-action a {
text-transform: uppercase;
}
</style>
<div id="sidebar-nav" class="sidebar">
    <div class="sidebar-scroll">
        <nav>
            <ul class="nav">
                <li><a href="index.php" class=""><i class="lnr lnr-home"></i> <span>Dashboard</span></a>
                </li>
                <li><a href="chat.php" class="active"><i class="lnr lnr-envelope"></i> <span>Messages</span></a>
                </li>

                <li><a href="parametres.php" class=""><i class="lnr lnr-cog"></i> <span>Paramètres</span></a></li>
            </ul>
        </nav>
    </div>
</div>
<!-- END LEFT SIDEBAR -->
<!-- MAIN -->
<div class="main">
    <!-- MAIN CONTENT -->
    <div class="main-content">
      <div class="container">
<div class="row">
  <?php
      $id=$_SESSION['id'];
      $sql = "SELECT DISTINCT id_s FROM commentaire WHERE id_u='$id' AND types=1 ORDER BY id DESC";
      $result = $conn->query($sql);
      if ($result->rowCount() > 0) {
      while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
        $ids=$row["id_s"];
        $sql1="SELECT * FROM services WHERE id='$ids'";
        $resp1=$conn->query($sql1);
        $row1 = $resp1->fetch(PDO::FETCH_ASSOC);
        $idp=$row1['fk_p_s'];
        $categorie=$row1['categorie'];
        $metier=$row1['metier'];
        $req2="SELECT * FROM users WHERE id='$idp'";
        $response2=$conn->query($req2);
        $ligne = $response2->fetch(PDO::FETCH_ASSOC);
        $ida=$ligne['id'];
        $ville=$ligne['ville'];
        $mail=$ligne['email'];
        $phone=$ligne['phone'];

        $s1="SELECT * FROM secteur WHERE id='$metier'";
        $r1=$conn->query($s1);
        $r2 = $r1->fetch(PDO::FETCH_ASSOC);

        $s="SELECT * FROM categorie WHERE id='$categorie'";
        $r=$conn->query($s);
        $r1 = $r->fetch(PDO::FETCH_ASSOC);


        ?>
        <div class="col-lg-3">
            <a href="message.php?ids=<?php echo $ids;?>&idu=<?php echo $ida;?>" class="sms" style="color:#383A3B">

              <div class="card  -margin">
                  <div class="card-body pt-0">
                      <div class="widget-49">
                          <div class="widget-49-title-wrapper">
                              <div class="widget-49-date-primary">
                                <img src="assets/img/engineer.png" style="height:25px" alt="">
                              </div>
                              <div class="widget-49-meeting-info">
                                <span class="widget-49-pro-title" style="font-family:lato;font-size:16px;padding-top:12px"><?php echo $ligne['lastname'].' '.$ligne['firstname'];?></span>
                                <span class="widget-49-pro-title" style="font-family:lato;font-size:16px;"><?php echo strtolower($r2['nom_secteur']);;?></span>
                                <div style="border:0.5px solid #2B79A0">

                                </div>
                              </div>
                          </div>
                          <br>
                          <ul class="b" style="padding-left:65px;font-family:lato;">
                            <li> <?php echo $mail;?></li>
                            <li> <?php echo $phone;?></li>
                            <li> <?php echo $ville;?></li>
                          </ul>
                          <div class="widget-49-meeting-action">

                              <a href="message.php?ids=<?php echo $ids;?>&idu=<?php echo $ida;?>" class="btn btn-sm btn-flash-border-primary sms"   style="font-family:lato"><?php
                              $idu=$_SESSION['id'];

                              $stmt = "SELECT COUNT(*) AS nb FROM commentaire WHERE id_u='$idu' AND id_s='$ids' AND SendRenceive='R' AND etat='0'";
                              $result1=$conn->query($stmt);
                              $row = $result1->fetch();
                              echo $row['nb'];

                              ?> Message(s) non lus&ensp;<span class="fa fa-long-arrow-right"></span></a>
                          </div>
                      </div>
                  </div>
              </div>

            </a>
        </div>

        <?php


      }}
  ?>


</div>
</div>
    </div>
    <!-- END MAIN CONTENT -->
</div>
<!-- END MAIN -->
<div class="clearfix"></div>

</div>



<script type="text/javascript">

setInterval(function(){ $.ajax({
    url: 'refrech.php',
    type: 'GET',
    data: 'idu=' + <?php echo $_SESSION['id'];?>,
    dataType: 'json',
    success: function(data) {

            $(".nbrc").html(
                '<sub>'+data[0]['nb']+'<sub>'

            );

    },




});}, 1);
</script>

<script src="javascript/chat.js"></script>
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/scripts/klorofil-common.js"></script>
</body>

</html>
