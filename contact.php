<?php include 'header/header.php'?>


    <!-- Hero Section End -->

    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="./assets/img/blog/w.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2 style="color:#144457;font-family: 'Texturina', serif;">CONTACT</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Blog Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_phone"></span>
                        <h4>Téléphone</h4>
                        <p>+237 6 20 03 75 43
</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_pin_alt"></span>
                        <h4>Addresse</h4>
                        <p>709 Yaoundé, Acacia</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_clock_alt"></span>
                        <h4>Ouverture</h4>
                        <p>24h/24 - 7j/7</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_mail_alt"></span>
                        <h4>Email</h4>
                        <p>infos@onsebat.com</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section End -->

    <!-- Map Begin -->
    <div class="map">
        <iframe
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15923.447040933237!2d11.4877037!3d3.8398386!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4050fbb4a831cda2!2sBusiness%20Intelligence%20Agency%20Sas!5e0!3m2!1sfr!2scm!4v1622918598096!5m2!1sfr!2scm" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        <div class="map-inside">
            <i class="icon_pin"></i>
            <div class="inside-widget">
                <h4>Yaoundé</h4>
                <ul>
                    <li>Téléphone: +237 6 20 03 75 43</li>
                    <li>Addresse: 709 Yaoundé, Acacia</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Map End -->

    <!-- Contact Form Begin -->
    <div class="contact-form spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="contact__form__title">
                        <h2>Laissez un message
</h2>
                    </div>
                </div>
            </div>
            <form action="#">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <input type="text" placeholder="Votre nom">
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <input type="text" placeholder="Votre Email">
                    </div>
                    <div class="col-lg-12 text-center">
                        <textarea placeholder="Votre  message"></textarea>
                        <button type="submit" class="site-btn">ENVOYER MESSAGE</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Contact Form End -->

    <!-- Blog Section End -->
    <?php include 'footer/footer.php';?>
   
</body>

</html>