<?php
$service_key='';
$cout ='';

if (isset($_POST['service_key']) && isset($_POST['cout']) ){
    $service_key=htmlentities($_POST['service_key']);
    $cout=htmlentities($_POST['cout']);
}


$return_url = '';
$notify_url = '';

$monetbil_args = array(
    'amount' => $cout,
    'logo' => 'http://spacekola.com/web/assets/img/logo01.png'

);

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL, 'https://www.monetbil.com/widget/v2.1/' . $service_key);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($monetbil_args, '', '&'));

$response = curl_exec($ch);

echo curl_error($ch);

curl_close($ch);
$result = json_decode($response, true);

$payment_url = '';
if (is_array($result) and array_key_exists('payment_url', $result)) {
$payment_url = $result['payment_url'];
}
?>
<div class="container">
    <div class="row">
        <div class="col-md-5 " style="border: 1px solid #c8eedb;height: 285px">
            <img src="Ressource/views/agregateur/img/monet.png" style="width: 90%;padding-top:25%">
        </div>
        <div class="col-md-7">
            <form class="form-group" method="POST" style="border: 1px solid #9de0f6;padding: 20px;height: 285px">
                <input type="text" style="height: 45px;" name="service_key" class="form-control" required  placeholder="Service key" ><br>
                <input type="text" style="height: 45px;" name="cout" class="form-control" required  placeholder="Montant"><br>
                <div class="input-group mb-2">
                    <!-- Target -->
                        <input type="text" style="height: 45px;" class="form-control" id="inlineFormInputGroup"  disabled  placeholder="API" value="<?php echo $payment_url?>">
                        <div class="input-group-prepend">
                            <span class="copy btn btn-primary" data-clipboard-action="copy" data-clipboard-target="#inlineFormInputGroup"><span class="fa fa-copy" style="padding-top: 10px">&ensp;Copy</span></span>
                        </div>
                    </div><br>
                <input type="submit" class="btn btn-danger text-uppercase btn-lg" style="float: right" value="generer API"><br><br>
            </form>

    </div>
</div>
</div>
<script src="Ressource/views/agregateur/clipboard.min.js"></script>
<script>
    var clipboard = new ClipboardJS('.copy');

    if (document.getElementById('inlineFormInputGroup').value !=''){
        document.getElementById('inlineFormInputGroup').disabled=false;
    }
    else {
    }


</script>