<?php


use Illuminate\Support\Facades\Route;

class AgregateurController extends Controller{


    public function listView($next = 1, $per_page = 10){
        $this->entitytarget = 'Agregateur';
        $this->title = "Manage Agregateur";
        $this->renderListView2();

    }
    public static function count(){
        return Agregateur::count();
    }

    public function listAgregateur(){
       return array(
           'success'=>true,
           'agregateurs'=>Agregateur::all('nom')
       ) ;
        //["success" => true, "agregateurs"=>Agregateur::all("nom")];
    }

    public function route($agregateur){
        ob_start();
        switch ($agregateur){
            case 'monetbil':
                $this->monetbil();
                break;
            case 'dohone':
                $this->dohone();
                break;
            case 'wecashup':
                $this->wecashup();
                break;
            case 'paypal':
                $this->paypal();
                break;
        }
        $a=array('contenu'=>ob_get_clean());
        Genesis::renderView("agregateur.template",$a);
    }
    public function monetbil(){
        Genesis::renderView("agregateur.monetbil");
    }
    public function dohone(){
        Genesis::renderView("agregateur.dohone", []);
    }
    public function wecashup(){
        Genesis::renderView("agregateur.wecashup", []);
    }
    public function paypal(){
        Genesis::renderView("agregateur.paypal", []);
    }
}
