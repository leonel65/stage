<?php include 'header/header.php';
$id=$_GET['id'];
?>
<script src="prestataire/home/js/ckeditor/ckeditor.js"></script>

<section class="product-details spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <div class="product__details__pic">
                    <div class="product__details__pic__item">
                        <img height="300" class="product__details__pic__item--large" src="

<?php

$id=$id.'_0';
$stmt = "SELECT url_img FROM img WHERE url_img LIKE '$id%' LIMIT 1";
$result1=$conn->query($stmt);
$row = $result1->fetch(PDO::FETCH_ASSOC);
echo 'prestataire/home/uploads/'.$row['url_img'];?>
" alt="" data-pagespeed-url-hash="232668656" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    </div>
                    <div class="product__details__pic__slider owl-carousel">

                        <?php
    $id=$_GET['id'];
    $sql = "SELECT url_img FROM img WHERE url_img LIKE '$id%'";
    $result = $conn->query($sql);
    if ($result->rowCount() > 0) {
        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            ?>
                        <img width="150" height="100"
                            data-imgbigurl="prestataire/home/uploads/<?php echo $row['url_img'];?>"
                            src="prestataire/home/uploads/<?php echo $row['url_img'];?>" alt=""
                            data-pagespeed-url-hash=""
                            onload="pagespeed.CriticalImages.checkImageForCriticality(this);">

                        <?php
        }
    }

    ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="product__details__text">
                    <h3><?php
    $sql="SELECT * FROM services WHERE id='$id'";
    $resp=$conn->query($sql);
    $row = $resp->fetch(PDO::FETCH_ASSOC);
    $exp=$row['a_exp'];
    $description=$row['description'];
    $enregistrer=$row['date_creation'];
    $idp=$row['fk_p_s'];
    $categorie=$row['categorie'];
    $metier=$row['metier'];
    $req="SELECT * FROM users WHERE id='$idp'";
    $response=$conn->query($req);
    $ligne = $response->fetch(PDO::FETCH_ASSOC);
    $ville=$ligne['ville'];
    $mail=$ligne['email'];
    $phone=$ligne['phone'];
    echo $ligne['lastname'].' '.$ligne['firstname'];
?></h3>
                    <div class="product__details__rating" style='margin-bottom:-30px'>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </div>

                    <ul>
                        <li><b>Secteur d'activité : </b> <span>
                                <?php
    $s="SELECT * FROM categorie WHERE id='$categorie'";
    $r=$conn->query($s);
    $r1 = $r->fetch(PDO::FETCH_ASSOC);
    echo strtoupper($r1['nom_categorie']);
    ?>
                            </span></li>
                        <li><b>Metier : </b> <span>
                                <?php
    $s1="SELECT * FROM secteur WHERE id='$metier'";
    $r1=$conn->query($s1);
    $r2 = $r1->fetch(PDO::FETCH_ASSOC);
    echo strtolower($r2['nom_secteur']);
    ?>
                            </span></li>
                        <li><b>Année d'expérience:</b> <span><?php if ($exp=='0') {
   echo $exp. ' an';
}else {
    echo $exp. ' ans';
}?></span></li>
                        <li><b>Lieu de residence :</b> <span><?php echo $ville;?></span></li>
                        <li><b>Adresse mail :</b> <span><?php echo $mail;?></span></li>
                        <li><b>Contact: </b> <span><?php echo $phone;?> <a style="color:teal"
                                    href="https://wa.me/<?php echo $phone?>" class="whatsapp_float" target="_blank">
                                    &ensp; <i class="fa fa-whatsapp whatsapp-icon"
                                        style="font-size:20px"></i></a></span></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="product__details__tab">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                aria-selected="true">Description</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link allComment" data-toggle="tab" href="#tabs-3" role="tab"
                                aria-selected="false">Commentaires
                                <span></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="compte/message.php?ids=<?php echo $_GET['id'];?>&idu=<?php echo $idp;?>" role="tab" aria-selected="true">Message
                                privée</a>
                        </li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabs-1" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <?php echo $description; ?>
                            </div>
                        </div>


                        <div class="tab-pane" id="tabs-3" role="tabpanel">
                            <div class="product__details__tab__desc">
                                <div class="container">
                                    <div class="col-lg-12 col-sm-12 text-center">
                                        <div class="well">

                                            <div class="input-group">
                                                <input type="text" id="userComment"
                                                    class="form-control input-lg chat-input"
                                                    placeholder="Écrire le commentaire" />

                                                <span class="input-group-btn" >&ensp;
                                                    <a class="btn btn-primary btn-md comment" style="color: white"><span
                                                            class="fa fa-pencil"></span>Ajouter
                                                        </a>
                                                </span>
                                            </div>
                                            <hr data-brackets-id="12673">
                                            <ul data-brackets-id="12674" id="sortable"
                                                style="height:350px;" class="overflow-auto list-unstyled ui-sortable">


                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include 'footer/footer.php'; ?>
<script>
$(document).ready(function() {


$(".allComment").click(function() {
  var ids=<?php echo $_GET['id'];?>;
  setInterval(function(){ $.ajax({
      url: 'allComment.php',
      type: 'GET',
      data: 'ids=' + ids,
      dataType: 'json',
      success: function(data, i) {
          $("#sortable").empty();
          for (i = 0; i < data.length; i++) {
            data[i]['id_u']

              $("#sortable").append(
                  '<strong class="pull-left primary-font">Utilisateur '+data[i]['id_u']+'</strong>'+
                  '<small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span> le '+data[i]['datep']+'</small></br>'+
                  '<li class="text-left">'+data[i]['commentaires']+'</li><hr>'

              );
          }
      },




  });}, 1);


})
    $(".comment").click(function() {
        var userComment = $("#userComment").val();
        var lastname='<?php echo $_SESSION['lastname'];?>'
        var d = new Date();
        var hours = d.getHours() + " h " + d.getMinutes()+' s';
        if (userComment != '') {
          var data='<strong class="pull-left primary-font">'+lastname+'</strong>'+
                   '<small class="pull-right text-muted"><span class="glyphicon glyphicon-time"></span>'+'a '+hours+'</small><br>'+
                   '<li class="text-left">'+userComment+'</li></br><hr>';
            $("#sortable").prepend(data)
            $('#userComment').val('').focus();
            $.ajax({
                url: 'comment.php',
                type: 'GET',
                data: 'message=' + userComment+'&idu='+<?php echo $_SESSION['id'];?>+'&ids='+<?php echo $_GET['id'];?>,
                dataType: 'json',
                success: function() {


                },




            });
        }
    })


});
</script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script>
CKEDITOR.replace('editor');
</script>
</body>

</html>
