@extends('layout')
@section('title', 'List')

@section('layout_content')

    <div class="row">
        <div class="col-lg-12 col-md-12  stretch-card">
            <div class="card">
                <div class="card-header-tab card-header">
                    {{$title}}
                    <div class="card-header-title">
                        <i class="header-icon lnr-rocket icon-gradient bg-tempting-azure"> </i>
                    </div>
                    <div class="btn-actions-pane-right">
                        <div class="nav">

                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 2%; margin-left: 10px;margin-bottom: 10px">
                       <div class="col-md-4">
                           <div class="card" style="width: 18rem;">
                               <div class="card-body">
                                   <h5 class="card-title">Monetbil</h5>
                                   <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                   <a href="https://www.monetbil.com" style="float: left;border: 1px solid #f78ca0; border-radius: 20px; padding: 6px" class="card-link">En savoir plus</a>
                                   <a href="" data-toggle="modal" data-target="#myModal" style="float: right;border: 1px solid #3f6ad8; border-radius: 20px; padding: 6px" class="card-link">Continuer</a>
                               </div>
                           </div>
                       </div>
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Dohone</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="https://www.my-dohone.com/dohone/" style="float: left;border: 1px solid #f78ca0; border-radius: 20px; padding: 6px" class="card-link">En savoir plus</a>
                                <a href="#" style="float: right;border: 1px solid #3f6ad8; border-radius: 20px; padding: 6px" class="card-link">Continuer</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">WeCashUp</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="https://www.wecashup.com" style="float: left;border: 1px solid #f78ca0; border-radius: 20px; padding: 6px" class="card-link">En savoir plus</a>
                                <a href="#" style="float: right;border: 1px solid #3f6ad8; border-radius: 20px; padding: 6px" class="card-link">Continuer</a>
                            </div>
                        </div><br>
                    </div>

                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Paypal</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="" style="float: left;border: 1px solid #f78ca0; border-radius: 20px; padding: 6px" class="card-link">En savoir plus</a>
                                <a href="#" style="float: right;border: 1px solid #3f6ad8; border-radius: 20px; padding: 6px" class="card-link">Continuer</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title">Stripe</h5>
                                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                <a href="https://stripe.com/fr" style="float: left;border: 1px solid #f78ca0; border-radius: 20px; padding: 6px" class="card-link">En savoir plus</a>
                                <a href="#" style="float: right;border: 1px solid #3f6ad8; border-radius: 20px; padding: 6px" class="card-link">Continuer></a>
                            </div>
                        </div>
                    </div>
                    </div>
                   </div>
                </div>
            </div>
        </div>


    <div id="{{ strtolower($entity) }}box" class="swal2-container swal2-fade swal2-shown" style="display:none; overflow-y: auto;">
        <div role="dialog" aria-labelledby="swal2-title" aria-describedby="swal2-content" class="swal2-modal swal2-show dv_modal" tabindex="1"
             style="">
            <div class="main-card mb-3 card  box-container">
                <div class="card-header">.

                    <button onclick="model._dismissmodal()" type="button" class="swal2-close" aria-label="Close this dialog" style="display: block;">×</button>
                </div>
                <div class="card-body"></div>
            </div>

        </div>
    </div>

@endsection

<?php function modalview($entity){ ?>

<div class="modal fade" id="{{ strtolower($entity) }}modal" tabindex="-1" role="dialog" aria-labelledby="modallabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="title" id="modallabel">Modal Label</h3>
            </div>
            <div class="modal-body panel generalinformation"></div>
            <div class="modal-footer">
                <button onclick="model._dismissmodal()" data-dismiss="modal" aria-label="Close" type="button" class="btn btn-danger">Close</button>
            </div>
        </div>
    </div>
</div>
<?php } ?>
