<?php
$conn = new PDO("mysql:host=localhost;dbname=stage", 'root', '');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

require('../../vendor/autoload.php');
require('../../vendor/phpmailer/phpmailer/src/Exception.php');
require('../../vendor/phpmailer/phpmailer/src/PHPMailer.php');
require('../../vendor/phpmailer/phpmailer/src/SMTP.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer;   

  $err=array();
  $insert=false;
  $secteur="";
  $domaine="";
  $aexperience="";
  $fileName="";
  $jrealisation="";
  $description="";
  $message="";




  if (isset($_POST['service'])) {
    
    $secteur=$_POST['secteur'];
    $domaine=$_POST['domaine'];
    $aexperience=$_POST['aexperience'];


    $fileName=$_FILES['jprofession']['name'];
    $fileSize=$_FILES['jprofession']['size'];
    $file_ext=strrchr($fileName,".");
    $extention_autorise=array('.pdf','.PDF','.jpeg','.JPEG','.jpg','.jpg','.png','.PNG');



    $jrealisation=$_FILES['realisation']['name'];
    $jrealisationSize=$_FILES['realisation']['size'];
    $jrealisation_autorisé=array('.jpeg','.JPEG','.jpg','.jpg','.png','.PNG');
    $jrealisation_num=count($jrealisation);


    $description=$_POST['edit'];
    
    if ($fileSize>3000000) {
                    $err['size'] = "La taille du justificatif de la profession  doit etre inférieur a 3M";

    }
  
    if (in_array($file_ext,$extention_autorise)) {
      
      $id=$_SESSION['id'];
      $sect=strtolower($secteur);
      $file=$fileName;
      $today = date("Y-m-d H:i:s");
       $sql1 = "SELECT * FROM services WHERE fk_p_s='$id' AND metier='$domaine'";
                      $result1 = $conn->query($sql1);
                       if ($result1->rowCount() > 0) {
        $err['metier'] = "Ce service existe déja";

                       }else{
      $sql = "INSERT INTO services (file_just,a_exp,fk_p_s,categorie,metier,description,date_creation,status)
      VALUES ('$file','$aexperience',$id,'$sect','$domaine','$description','$today','0')";

      if($conn->exec($sql)){
        $last_id = $conn->lastInsertId();
                        $new_file=$last_id.'_'.$file;

        move_uploaded_file($_FILES['jprofession']['tmp_name'], "uploads/".$new_file);
        $upt = "UPDATE services SET file_just='$new_file' WHERE id=$last_id";
        $stmt = $conn->prepare($upt);
        $stmt->execute();
        $insert=true;
 
      }
      else {
        $err['data'] = "Erreur lors de l'enregistrement dans la base de donnée";
      }
                       }
      
    } else {
        $err['format'] = "Format du justificatif de la profession non autorisé";
    }






      
if ($insert) {

      for ($i=0; $i < $jrealisation_num; $i++) { 

        $realName=$_FILES['realisation']['name'][$i];
        $realSize=$_FILES['realisation']['size'][$i];
        $realExt=strrchr($realName,".");
        $ext_autorise=array('.jpeg','.JPEG','.jpg','.jpg','.png','.PNG'); 

        if ($realSize>3000000) {
                    $err['size'] = "La taille de l'image". $i." doit etre inférieur a 3M";

        }

        if (in_array($realExt,$ext_autorise)) {
          $new2=$last_id.'_'.$i.'_'.$realName;
          $sql2 = "INSERT INTO img (url_img,fk_s)
          VALUES ('$new2',$last_id)";
          if($conn->exec($sql2)){

            move_uploaded_file($_FILES['realisation']['tmp_name'][$i], "uploads/".$new2);
            
            $mail = new PHPMailer;
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host       = 'smtp.gmail.com';
            $mail->SMTPAuth   = true;
            $mail->Username   = 'serviceonsebat@gmail.com';
            $mail->Password   = 'Benediction100-';
            $mail->SMTPSecure = 'ssl';
            $mail->Port       = 465;

            //Recipients
            $mail->setFrom('serviceonsebat@gmail.com');
            $mail->addAddress($email);

            // Content
            $mail->isHTML(true);
            $mail->Subject = 'Création de service';
            $mail->Body    =  "
            <html>
                <body>
                    <h2>Merci pour votre inscription !</h2><br>
                    <p>Bonjour</p><br>
                    <p>Votre service a été créé et est actuellement en cours de tratement, nous reviendront vers vous d'ici peu </p>
                    <p>Merci, et à bientôt !</p>
                    <p>Service Onsebat</p>
                    </body>
            </html>";
            if ($mail->Send()) {
              ?>
<script>
alert('Votre service a été crée');
</script>
<?php
            }

          }else{

            $err['data2'] = "Erreur lors de l'enregistrement des images dans la base de donnée";
          }
          }else {


            $err['format'] = "seul les fichier d'extention .png, .jpg .jpeg sont pris en compte";
          
          }   
        
      }
}

    
 

        
}

 

function valid_donnees($donnees){
    $donnees = trim($donnees);
    $donnees = stripslashes($donnees);
    $donnees = htmlspecialchars($donnees);
    return $donnees;
}
?>