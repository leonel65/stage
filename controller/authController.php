<?php
session_start();
include_once 'config/dbconnection.php';
require('vendor/autoload.php');
require('vendor/phpmailer/phpmailer/src/Exception.php');
require('vendor/phpmailer/phpmailer/src/PHPMailer.php');
require('vendor/phpmailer/phpmailer/src/SMTP.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer;                              // Passing `true` enables exceptions

$err = array();
$login = false;
$firstname = "";
$lastname = "";
$phone = "";
$email = "";
$e = "";
$p = "";
$message = "";
$er="";
if (isset($_POST['creation'])) {
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    $cpass = $_POST['confirm'];
    if ($pass != $cpass) {
        $err['pass'] = "Les mots de passe ne sont pas identiques";
    }
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $err['email'] = "Adresse mail invalide";
    }

    $sql = "SELECT * FROM users WHERE email='$email' LIMIT 1";
    $result = $conn->query($sql);
    $usercount=$result->rowCount();
    
    if ($usercount > 0) {
        $err['email'] = "Adresse email déja utilisé";
    }

    if (count($err) === 0) {
        $pass = md5($pass);
        $token = bin2hex(random_bytes(50));
        $verifiel = 0;
        $role='1';
        $sql = "INSERT INTO users(firstname,lastname,email,phone,pwd,verified,token,role) VALUES('$firstname','$lastname','$email','$phone','$pass','$verifiel','$token','$role')";

        if ($conn->exec($sql)) {
           
            $_SESSION['firstname'] = $firstname;
            $_SESSION['lastname'] = $lastname;
            $_SESSION['email'] = $email;
            $_SESSION['phone'] = $phone;
            $_SESSION['verified'] = $verifiel;
            $_SESSION['token'] = $token;
            $_SESSION['message'] = $lastname . " " . $firstname . ", un mail vous a été envoyé a l'adresse " . $email . " pour l'activation de votre compte";


            $mail = new PHPMailer;
            $mail->SMTPDebug =0;
            $mail->CharSet = 'utf-8';   
            $mail->isSMTP();
            $mail->Host       = 'smtp.gmail.com';
            $mail->SMTPAuth   = true;
            $mail->Username   = 'infos.onsebat@gmail.com';
            $mail->Password   = 'Leonel65';
            $mail->SMTPSecure = 'ssl';
            $mail->Port       = 465;

            //Recipients
            $mail->setFrom('infos.onsebat@gmail.com');
            $mail->addAddress($email);

            // Content
            $mail->isHTML(true);
            $mail->Subject = 'Compte cree avec succes ! sur Onsebat.com';
            $mail->Body    =  "
            <html>
                <body>
                    <h2>Merci pour votre inscription !</h2><br>
                    <p>Bonjour " . $lastname . " " . $firstname . "</p><br>
                    <p>Votre compte a été créé avec succès sur Onsebat ! Veuillez cliquer sur le lien ci-dessous pour l'activer.</p>
                    <p>Cliquer <a href='http://localhost/stage/login.php?err=0&token=" . $token . "'>ici</a> Pour activer votre compte</p>
                    <p>Merci, et à bientôt !</p>
                    <p>Service Onsebat</p>
                    </body>
            </html>";

            if ($mail->Send()) {
              echo "<script>window.location.assign('step1.php')</script>";
            }
           
        } else {
            $err['database'] = "Erreur de la Base de donnée";
        }
        
    }

   
}

if (isset($_POST['login'])) {
    $e = htmlspecialchars($_POST['email']);
    $p = htmlspecialchars($_POST['password']);
    $p = md5($p);

    
    $sql = "SELECT * FROM users WHERE email='" . $e . "' AND pwd='" . $p . "'";
    $result = $conn->query($sql);
    if ($result->rowCount() === 1) {
        $row = $result->fetch(PDO::FETCH_ASSOC);
        if ($row['verified'] == 0) {
            header("location: login.php?token&err=1");
        } else {
            $_SESSION['connect']=true;
            $_SESSION['id'] = $row['id'];
            $_SESSION['firstname'] = $row['firstname'];
            $_SESSION['lastname'] = $row['lastname'];
            header("location: index.php");
        }
    } else {
        header("location: login.php?token&err=2");
    }


    
}
if (isset($_POST['recherche'])) {
     $search =htmlspecialchars($_POST['search']);
     if (empty($search)) {
         $er="Veuillez saisir l'élement a rechercher svp";
     }
     else {
        header("location: recherche.php?elt=$search");
     }


}