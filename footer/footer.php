<footer class="footer spad">
        <div class="container">
            <div class="row">
                
                <div class="col-md-3">
                    <div class="footer__widget">
                        <h8>A PROPOS</h8><br><br>
        <p style="font-family:Lato;text-align:justify">
        Onsebat est une plateforme de mise en contact entre des tierces personnes et des ouvriers(Techniciens). Chaque techniciens peut ouvrir un compte gratuitement, ajouter ses services afin d'etre contacté.

        </p>                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer__widget">
                        <h8>CONTACT</h8><br><br>
                        <p style="font-family:Lato"><span style="color:#7fad39" class="fa fa-envelope"></span> info@onsebat.com</p>
                        <p style="font-family:Lato"><span style="color:#7fad39" class="fa fa-archive"></span> BP 14893 Yaoundé - Cameroun</p>
                        <p style="font-family:Lato"><span style="color:#7fad39" class="fa fa-map-marker"></span> Carrefour Acacias, Biyem-Assi</p>

         
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer__widget">
                        <h8>LIENS UTILES</h8><br><br>
                        <p style="font-family:Lato"><span style="color:#7fad39" class="fa fa-arrow-circle-o-right"></span>&ensp;<a style="color:black" href="">Tout sur Onsebat</a></p>
                        <p style="font-family:Lato"><span style="color:#7fad39" class="fa fa-arrow-circle-o-right"></span>&ensp;<a style="color:black" href="">Comment ca marche?</a></p>
                        <p style="font-family:Lato"><span style="color:#7fad39" class="fa fa-arrow-circle-o-right"></span>&ensp;<a style="color:black" href="contact.php">Contacter nous</a></p>
                       
                    </div>
                </div>
                <div class="col-md-3">
                <div class="footer__widget">
                        <h8>FACEBOOK</h8><br><br>
                   <div class="fb-page" data-href="https://www.facebook.com/businessintelligenceagencycm" data-tabs="timeline" data-width="300" data-height="0" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/businessintelligenceagencycm" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/businessintelligenceagencycm">BIA</a></blockquote></div>
                   </div></div>
            </div>
        </div>
                        <p style="text-align:center;font-family:Lato"><i>Onsebat © 2020 All rights reserved | Auteur Groupe BIA</i><p>

    </footer>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="assets/js/jquery-3.3.1.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.nice-select.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <script src="assets/js/jquery.slicknav.js"></script>
    <script src="assets/js/mixitup.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script>
        $('.carousel').carousel({
            interval: 3000
        })
    </script>
    <script async defer crossorigin="anonymous"
        src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v10.0&appId=352022751942149&autoLogAppEvents=1"
        nonce="GTSd62SJ"></script>

</body>

</html>