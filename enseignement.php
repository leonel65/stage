<?php include 'header/header.php'?>

<section class="breadcrumb-section set-bg" data-setbg="./assets/img/blog/bib.jpg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="breadcrumb__text">
                    <h2 style="color:#144457;font-family: 'Texturina', serif;">ÉDUCATION</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<section style="margin-top: -65px;" class="blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3">
                <div class="blog__sidebar">

                    <div class="blog__sidebar__item" style="width:100px">
                        <h4>Metier(s)</h4>
                        <ul>
                            <li><a href="batiment.php?p=<?php echo $_GET['p']?>&m=">Tout (<span
                                        style="color:#0E2E4B;font-weight:bold"><?php
                                 $id=$_GET['p'];
                                $stmt = "SELECT COUNT(*) AS nb FROM services WHERE categorie='$id' AND status='1'"; 
                                $result1=$conn->query($stmt);
                                $row = $result1->fetch();
                                echo $row['nb'];
                                
                                ?></span>)</a></li>
                            <?php
                                       $id=$_GET['p'];
                    $sql = "SELECT * FROM secteur WHERE fk_s_c=$id";
                    $result = $conn->query($sql);
                    if ($result->rowCount() > 0) {
                      while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                        $id=$row["id"];
                        $nom=$row["nom_secteur"];
                        ?>
                            <li><a href="batiment.php?p=<?php echo $_GET['p']?>&m=<?php echo $id;?>"
                                    class='metier'><?php echo $nom;?>(<span style="font-weight:bold;color:#0E2E4B"><?php 
                                  $stmt = "SELECT COUNT(*) AS nb FROM services WHERE metier='$id' AND status='1'"; 
                                  $result1=$conn->query($stmt);
                                  $row = $result1->fetch();
                                  echo $row['nb'];
                            ?></span>)</a></li>
                            <?php
                      }
                    }
                    else {
                    }
                ?>

                        </ul>
                    </div>



                </div>
            </div>
            <div class="col-lg-9 col-md-9" style="padding-top:25px">
                <div class="row" id="data">

                    <?php
                            $id=$_GET['p'];
                            $metier=$_GET['m'];
                            if ($metier=='') {
                                $sql = "SELECT * FROM services WHERE categorie=$id AND status=1";
                            }
                            else {
                                $sql = "SELECT * FROM services WHERE categorie=$id AND metier='$metier' AND status=1";
                            }
                            $result = $conn->query($sql);
                            if ($result->rowCount() > 0) {
                              while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                                $ids=$row["id"];
                                $idc=$row["categorie"];
                                $idp=$row["fk_p_s"];
                                $idm=$row["metier"];
                                $ae=$row["a_exp"];

                                ?>

                    <div class="col-md-3"
                        style="margin-right:60px;margin-bottom:40px;border:2px solid #C3CCD5;padding:5px;border-radius:5px">
                        <a href="<?php 
                            if (isset($_SESSION['connect'])) {
                                echo 'details.php?id='.$ids;
                            } else {
                                echo 'login.php?token&err=0';
                            }
                            
                        ?>">
                            <div class="card-sl">
                                <div class="card-image" style="height:150px">
                                    <img style="background-size: contain, cover;" src="<?php
                                                        $id=$ids.'_0';
                                                        $stmt = "SELECT url_img FROM img WHERE url_img LIKE '$id%' LIMIT 1"; 
                                                        $result1=$conn->query($stmt);
                                                        $row = $result1->fetch(PDO::FETCH_ASSOC);
                                                        echo 'prestataire/home/uploads/'.$row['url_img'];
                                                        
                                                        ?>" />
                                </div>

                                <div class="card-text" style="color:black;">
                                    <p>Profession:
                                        <?php
                                 $stmt2 = "SELECT *  FROM secteur WHERE id='$idm' LIMIT 1"; 
                                                        $result3=$conn->query($stmt2);
                                                        $row2 = $result3->fetch(PDO::FETCH_ASSOC);
                                                        echo $row2['nom_secteur'];
                                ?>
                                    </p>
                                    <p>Nom:<?php
                                    
                                       $stmt1 = "SELECT *  FROM users WHERE id='$idp' LIMIT 1"; 
                                                        $result2=$conn->query($stmt1);
                                                        $row1 = $result2->fetch(PDO::FETCH_ASSOC);
                                                        $firstname=$row1['firstname'];
                                                        $lastname=$row1['lastname'];
                                                        $ville=$row1['ville'];

                                                        echo $lastname .' '. $firstname; 
                                    
                                    ?></p>
                                    <p>Expérience: <?php if ($ae=='0') {
                                        echo $ae .' an';
                                    }else{echo $ae. ' ans';}?></p>
                                    <p>Ville: <?php echo $ville;?></p>
                                </div>
                                <a href="#" style="float:right;color:#0E2E4B" class="card-button">Consulter&ensp;<span
                                        class="fa fa-arrow-right"></span></a>
                            </div>
                        </a>
                    </div>
                    <?php
                              }
                            }
                            else {
                               ?>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 offset-4">
                                Aucun technicien dans cette catégorie
                            </div>
                        </div>
                    </div>
                    <?php 
                            }
                      ?>


                </div>
            </div>
        </div>
</section>
<!-- Blog Section End -->
<?php include 'footer/footer.php';?>
<script>
function datacatégorie(id) {
    $.ajax({
        url: 'fetch.php',
        type: 'GET',
        data: 'id=' + id,
        dataType: 'json',
        success: function(data, i) {
            $("#data").empty();

            for (i = 0; i < data.length; i++) {

                $("#data").append(
                    "<p>" + data[i]['id'] + "</p>"
                );
            }
        },




    });
}
</script>

</body>

</html>