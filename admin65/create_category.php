<?php include 'header.php';?>

<div class="main-panel">
    <div class="content-wrapper">
        <form method="post" action="create_category.php">
            <div class="row">
                <div class="col-md-8 offset-1">
                    <div class="form-group">
                        <input type="text" class="form-control" required placeholder="Entrer le nom du secteur"
                            aria-label="Username" name="categorie">
                    </div>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-info mb-2" id="ajouter" name="AjouterC">Ajouter</button>
                </div>
        </form>
    </div>

    <div class="row" style="padding: 25px;">
        <div class="table-responsive">
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th style="font-weight: bold;">N <sup>o</sup> </th>
                        <th style="font-weight: bold;">Secteur</th>
                        <th style="font-weight: bold;">Date création</th>
                        <th style="font-weight: bold;">Statut</th>
                        <th style="font-weight: bold;">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                      $sql = "SELECT id, nom_categorie,creation, statut FROM categorie ORDER BY id DESC";
                      $result = $conn->query($sql);
                      if ($result->rowCount() > 0) {
                        $i=1;
                        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                          $id=$row["id"];
                          $nom_categorie=$row["nom_categorie"];
                          $creation=$row["creation"];
                          $statut=$row["statut"];
                          ?>
                    <tr>
                        <td><?php echo $i++ ?></td>
                        <td><?php echo $nom_categorie ?></td>
                        <td><?php echo $creation; ?></td>
                        <td><?php
                            if ($statut==0) {
                                echo "<button type='button' class='btn btn-social-icon btn-outline-linkedin statut'><i class='mdi  mdi mdi-check'></i></button>&ensp;";
                            } else {
                                echo "<button type='button' class='btn btn-social-icon btn-outline-dribbble statut'><i class='mdi  mdi mdi-close'></i></button>&ensp;";
                            }
                                                          
                         ?></td>
                        <td><?php
                            echo "<button type='button' class='btn btn-social-icon btn-outline-twitter'><i class='mdi  mdi-lead-pencil'></i></button>";
                          ?></td>
                    </tr>
                    <?php
                        }
                      }
                      else {
                      }
                  ?>

            </table>
        </div>
    </div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({

        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        },

    });
});
</script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

<script src="../assets/toastr/toastr.min.js"></script>
<script>
document.getElementById('ajouter').addEventListener('click', function() {

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    toastr["success"]("Le service a été validé avec success")

})
</script>

<script src="../prestataire/home/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../prestataire/home/vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<scr src="../prestataire/home/js/off-canvas.js"></scr>
<script src="../prestataire/home/js/hoverable-collapse.js"></script>
<script src="../prestataire/home/js/template.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="../prestataire/home/js/dashboard.js"></script>
<script>
function notification() {
    window.location.assign('notification.php')
    return false;
}
</script>
</body>

</html>