<?php include 'header.php';?>

<div class="main-panel">
    <div class="content-wrapper">
        <div class="row" style="padding: 25px;">
            <div class="table-responsive">
                <table id="example" class="table table-striped">
                    <thead>
                        <tr>
                            <th>N <sup>o</sup> </th>
                            <th>Nom</th>
                            <th>Prenom</th>
                            <th>email</th>
                            <th>Telephone</th>
                            <th>État</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    $sql = "SELECT * FROM users WHERE role=1 ORDER BY id DESC";
                    $result = $conn->query($sql);
                    if ($result->rowCount() > 0) {
                      $i=1;
                      while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                        $id=$row["id"];
                        $nom=$row["firstname"];
                        $prenom=$row["lastname"];
                        $email=$row["email"];
                        $telephone=$row["phone"];
                        $etat=$row["verified"];
                        ?>
                        <tr>
                            <td><?php echo $i++; ?></td>
                            <td><?php echo $nom; ?></td>
                            <td><?php echo $prenom; ?></td>
                            <td><?php echo $email; ?></td>
                            <td><?php echo $telephone; ?></td>
                            <td><?php
                            if ($etat==1) {
                                echo "<button type='button' class='btn btn-social-icon btn-outline-linkedin'><i class='mdi  mdi mdi-check'></i></button>&ensp;";
                            } else {
                                echo "<button type='button' class='btn btn-social-icon btn-outline-dribbble'><i class='mdi  mdi mdi-close'></i></button>&ensp;";
                            }
                                                          
                         ?></td>

                        </tr>
                        <?php
                      }
                    }
                    else {
                    }
                ?>

                </table>
            </div>
        </div>
    </div>
</div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({

        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        },

    });
});
</script>

<script src="../prestataire/home/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../prestataire/home/vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<scr src="../prestataire/home/js/off-canvas.js"></scr>
<script src="../prestataire/home/js/hoverable-collapse.js"></script>
<script src="../prestataire/home/js/template.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="../prestataire/home/js/dashboard.js"></script>
<script>
function notification() {
    window.location.assign('notification.php')
    return false;
}
</script>
</body>

</html>