<?php include 'header.php';?>

<div class="main-panel">
    <div class="content-wrapper">
        <form method="post" action="create_work.php">
            <div class="row">
                <div class="col-md-4 offset-1">
                    <div class="form-group row">
                        <select class="form-control" required name="categorie">
                            <option disabled="" selected="" value="">Selectionner le secteur </option>

                            <?php

                    $sql = "SELECT nom_categorie FROM categorie";
                    $result = $conn->query($sql);
                    if ($result->rowCount() > 0) {
                      while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                        $categorie=$row['nom_categorie'];?>
                            <option><?php echo strtoupper($categorie); ?></option>
                            <?php
                      }

                    }
                  ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4 offset-1">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Saisir la profession" name="secteur"
                            required aria-label="Username">
                    </div>
                </div>
                <div class="col-md-1">
                    <button type="submit" class="btn btn-info mb-2" name="ajouterS">Ajouter</button>
                </div>
            </div>
        </form>
        <div class="row" style="padding: 25px;">
            <div class="table-responsive">
                <table id="example" class="table table-striped">
                    <thead>
                        <tr>
                            <th style="font-weight: bold;">N <sup>0</sup></th>
                            <th style="font-weight: bold;">Secteur</th>
                            <th style="font-weight: bold;">Profession</th>
                            <th style="font-weight: bold;">Date création</th>
                            <th style="font-weight: bold;">Statut</th>
                            <th style="font-weight: bold;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                    $sql = "SELECT id, nom_secteur,creation, statut, fk_s_c FROM secteur ORDER BY id DESC";
                    $result = $conn->query($sql);
                    if ($result->rowCount() > 0) {
                      $i=1;
                      while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                        $id=$row["id"];
                        $nom_secteur=$row["nom_secteur"];
                        $creation=$row["creation"];
                        $statut=$row["statut"];
                        $fk_s_c=$row['fk_s_c'];
                        $sql1 = "SELECT nom_categorie FROM categorie WHERE id='".$fk_s_c."'";
                        $result1 = $conn->query($sql1);
                        $data=$result1->fetch(PDO::FETCH_ASSOC);
                        $nom_categorie=$data['nom_categorie'];
                        ?>
                        <tr>
                            <td><?php echo $i++ ?></td>
                            <td><?php echo $nom_categorie; ?></td>
                            <td><?php echo $nom_secteur ?></td>
                            <td><?php echo $creation ?></td>
                            <td><?php
                           echo "<button type='button' class='btn btn-social-icon btn-outline-dribbble'><i class='mdi  mdi mdi-check'></i></button>&ensp;";
                         ?></td>
                            <td><?php
                           echo "<button type='button' class='btn btn-social-icon btn-outline-twitter'><i class='mdi  mdi-lead-pencil'></i></button>&ensp;";
                         ?></td>
                        </tr>
                        <?php
                      }
                    }
                    else {
                    }
                ?>

                </table>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({

        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        },

    });
});
</script>

<script src="../prestataire/home/vendors/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../prestataire/home/vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<scr src="../prestataire/home/js/off-canvas.js"></scr>
<script src="../prestataire/home/js/hoverable-collapse.js"></script>
<script src="../prestataire/home/js/template.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="../prestataire/home/js/dashboard.js"></script>
<script>
function notification() {
    window.location.assign('notification.php')
    return false;
}
</script>
</body>

</html>