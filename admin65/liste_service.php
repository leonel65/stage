<?php include 'header.php';?>

<div class="main-panel">
    <div class="content-wrapper">


        <div class="row" style="padding: 25px;">
            <div class="table-responsive">
                <table id="example" class="table table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th style="font-weight: bold;">N <sup>0</sup></th>
                            <th style="font-weight: bold;">Nom</th>
                            <th style="font-weight: bold;">Secteur</th>
                            <th style="font-weight: bold;">Profession</th>
                            <th style="font-weight: bold;">Creation</th>
                            <th style="font-weight: bold;">Opération</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                      $sql = "SELECT * FROM services WHERE status=1 ORDER BY id DESC";
                      $result = $conn->query($sql);
                      if ($result->rowCount() > 0) {
                        $i=1;
                        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                          $id=$row["id"];
                          $prestataire=$row["fk_p_s"];
                          $img_just=$row["file_just"];
                          $categorie=$row["categorie"];
                          $metier=$row["metier"];
                          $description=$row["description"];
                          $creation=$row["date_creation"];
                          ?>
                        <tr>
                            <td><?php echo $i++ ?></td>
                            <td><?php 
                          
                           $stmt = "SELECT firstname, lastname FROM users WHERE id='$prestataire' LIMIT 1"; 
                          $result1=$conn->query($stmt);
                          $row = $result1->fetch(PDO::FETCH_ASSOC);
                          echo $row['lastname'].' ' .$row['firstname'];

                          
                          ?></td>
                            <td><?php 
                          
                          $stmt = "SELECT nom_categorie FROM categorie WHERE id='$categorie' LIMIT 1"; 
                          $result1=$conn->query($stmt);
                          $row = $result1->fetch(PDO::FETCH_ASSOC);
                          echo $row['nom_categorie'];
                          
                          ?></td>
                            <td><?php
                          
                          $stmt = "SELECT nom_secteur FROM secteur WHERE id='$metier' LIMIT 1"; 
                          $result1=$conn->query($stmt);
                          $row = $result1->fetch(PDO::FETCH_ASSOC);
                          echo $row['nom_secteur'];

                          ?></td>
                            <td><?php echo $creation; ?></td>
                            <td><?php ?></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php
                        }
                      }
                      else {
                      }
                  ?>

                </table>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    $('#example').DataTable({

        "language": {
            "sEmptyTable": "Aucune donnée disponible dans le tableau",
            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
            "sInfoPostFix": "",
            "sInfoThousands": ",",
            "sLengthMenu": "Afficher _MENU_ éléments",
            "sLoadingRecords": "Chargement...",
            "sProcessing": "Traitement...",
            "sSearch": "Rechercher :",
            "sZeroRecords": "Aucun élément correspondant trouvé",
            "oPaginate": {
                "sFirst": "Premier",
                "sLast": "Dernier",
                "sNext": "Suivant",
                "sPrevious": "Précédent"
            },
            "oAria": {
                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
            },
            "select": {
                "rows": {
                    "_": "%d lignes sélectionnées",
                    "0": "Aucune ligne sélectionnée",
                    "1": "1 ligne sélectionnée"
                }
            }
        },

    });
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<script src="../prestataire/home/vendors/chart.js/Chart.min.js"></script>
<!-- End plugin js for this page-->
<!-- inject:js -->
<scr src="../prestataire/home/js/off-canvas.js"></scr>
<script src="../prestataire/home/js/hoverable-collapse.js"></script>
<script src="../prestataire/home/js/template.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<!-- End plugin js for this page -->
<!-- Custom js for this page-->
<script src="../prestataire/home/js/dashboard.js"></script>
<script>
function notification() {
    window.location.assign('notification.php')
    return false;
}
</script>
</body>

</html>