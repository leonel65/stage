<?php include 'header.php';?>

<div class="main-panel">
    <div class="content-wrapper">


        <div class="row" style="padding: 25px;">
            <div class="table-responsive">
                <table id="example" class="table table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th style="font-weight: bold;">N <sup>o</sup></th>
                            <th style="font-weight: bold;">Secteur</th>
                            <th style="font-weight: bold;">Profession</th>
                            <th style="font-weight: bold;">Année Exp</th>
                            <th style="font-weight: bold;">Just profession</th>
                            <th style="font-weight: bold;">Img réalisation</th>
                            <th style="font-weight: bold;">Description</th>
                            <th style="font-weight: bold;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                      $sql = "SELECT * FROM services WHERE status=0 ORDER BY date_creation DESC";
                      $result = $conn->query($sql);
                      if ($result->rowCount() > 0) {
                        $i=1;
                        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                          $id=$row["id"];
                          $categorie=$row["categorie"];
                          $img_just=$row["file_just"];
                          $metier=$row["metier"];
                          $ae=$row["a_exp"];
                          $description=$row["description"];
                          ?>
                        <tr id="elt<?php echo $id;?>">
                            <td><?php echo $i++ ?></td>
                            <td><?php 
                              $stmt1 = "SELECT nom_categorie FROM categorie WHERE id='$categorie' LIMIT 1"; 
                              $result2=$conn->query($stmt1);
                              $row2 = $result2->fetch(PDO::FETCH_ASSOC);
                          echo $row2['nom_categorie']; 
                          ?></td>
                            <td><?php
                          
                          $stmt = "SELECT nom_secteur FROM secteur WHERE id='$metier' LIMIT 1"; 
                          $result1=$conn->query($stmt);
                          $row = $result1->fetch(PDO::FETCH_ASSOC);
                          echo $row['nom_secteur'];
                          ?></td>
                            <td><?php echo $ae; ?></td>
                            <td style="text-align: center;"><a class="example-image-link"
                                    href="<?php echo '../prestataire/home/uploads/'.$img_just; ?>"
                                    data-lightbox="example-1" style="font-size:16px;">Visualiser</a></td>
                            <td>
                                <ul style="list-style-type: none">
                                    <?php
                                $a=$id;
                                $sql5 = "SELECT * FROM img WHERE fk_s='$id'";
                                $result5 = $conn->query($sql5);
                                $i=1;
                                while ($row5 = $result5->fetch(PDO::FETCH_ASSOC)) {                          
                                  ?>
                                    <li><a class="example-image-link"
                                            href="<?php echo '../prestataire/home/uploads/'.$row5['url_img']; ?>"
                                            data-lightbox="example-1"
                                            style="font-size:16px;">Image<?php echo '('.$i.')'?></a></li>
                                    <?php
                                  $i++;
                                } 
                            ?>
                                </ul>
                            </td>
                            <td><?php echo $description?></td>
                            <td><button class="btn btn-info btn-sm" id="valider"
                                    onclick="validate(<?php echo $id;?>)"><i class="mdi mdi-check "></i>
                                    Valider</button>&ensp;<button class="btn-sm btn btn-danger"><i
                                        class="mdi mdi-delete"></i>rejetter</button> </td>
                            </td>
                        </tr>
                        <?php
                        }
                      }
                      else {
                      }
                  ?>

                </table>


                <script>
                $(document).ready(function() {
                    $('#example').DataTable({

                        "language": {
                            "sEmptyTable": "Aucune donnée disponible dans le tableau",
                            "sInfo": "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                            "sInfoEmpty": "Affichage de l'élément 0 à 0 sur 0 élément",
                            "sInfoFiltered": "(filtré à partir de _MAX_ éléments au total)",
                            "sInfoPostFix": "",
                            "sInfoThousands": ",",
                            "sLengthMenu": "Afficher _MENU_ éléments",
                            "sLoadingRecords": "Chargement...",
                            "sProcessing": "Traitement...",
                            "sSearch": "Rechercher :",
                            "sZeroRecords": "Aucun élément correspondant trouvé",
                            "oPaginate": {
                                "sFirst": "Premier",
                                "sLast": "Dernier",
                                "sNext": "Suivant",
                                "sPrevious": "Précédent"
                            },
                            "oAria": {
                                "sSortAscending": ": activer pour trier la colonne par ordre croissant",
                                "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                            },
                            "select": {
                                "rows": {
                                    "_": "%d lignes sélectionnées",
                                    "0": "Aucune ligne sélectionnée",
                                    "1": "1 ligne sélectionnée"
                                }
                            }
                        },

                    });
                });
                </script>
                <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

                <script src="../assets/toastr/toastr.min.js"></script>

                <script>
                document.getElementById('valider').addEventListener('click', function() {
                    toastr.options = {
                        "closeButton": false,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-right",
                        "preventDuplicates": false,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    }

                    toastr["success"]("Le service a été validé!")


                })
                </script>

                <script>
                function validate(id) {
                    $.ajax({
                        url: 'validate.php', // La ressource ciblée
                        type: 'POST', // Le type de la requête HTTP.
                        data: {
                            id: id
                        },
                        success: function(response) {
                            if (response == 1) {
                                $('#elt' + id).hide()
                            }
                        }
                    });
                }
                </script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
                <!-- endinject -->
                <!-- Plugin js for this page-->
                <script src="../prestataire/home/vendors/chart.js/Chart.min.js"></script>
                <!-- End plugin js for this page-->
                <!-- inject:js -->
                <scr src="../prestataire/home/js/off-canvas.js"></scr>
                <script src="../prestataire/home/js/hoverable-collapse.js"></script>
                <script src="../prestataire/home/js/template.js"></script>
                <!-- endinject -->
                <!-- plugin js for this page -->
                <!-- End plugin js for this page -->
                <!-- Custom js for this page-->
                <script src="../prestataire/home/js/dashboard.js"></script>
                <script>
                function notification() {
                    window.location.assign('notification.php')
                    return false;
                }
                </script>
                </body>

                </html>