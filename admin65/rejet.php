  <?php
  session_start();
$conn = new PDO("mysql:host=localhost;dbname=stage", 'root', '');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            if ($_SESSION['admin']!="oui") {
              header("location:index.php");
              exit();
            }

      require_once '../controller/addCategorie.php';
  ?>


  <!DOCTYPE html>
  <html lang="en">

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Admin | secteur</title>
    <!-- base:css -->
    <link rel="stylesheet" href="../prestataire/home/vendors/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="../prestataire/home/vendors/css/vendor.bundle.base.css">
      <link rel="icon" type="image/png" href="../assets/img/first/o.png" />
    <link rel="stylesheet" href="../prestataire/home/css/style.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>

  </head>
  <body>
  <div>
  <?php echo $message;?>
  </div>
    <div class="container-scroller d-flex">
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item sidebar-category">
            <img src="../assets/img/first/logob.png" alt="">
            <span></span>
          </li>
                  <br><br>

          <li class="nav-item">
            <a class="nav-link" href="dashboard.php">
              <i class="mdi mdi mdi-view-dashboard menu-icon"></i>
              <span class="menu-title">Dashboard</span>
            </a>
          </li>
           <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
            <i class="mdi mdi-checkbox-multiple-blank-outline menu-icon"></i>
            <span class="menu-title">Services</span>
            <i class="menu-arrow"></i>
          </a>
          <div class="collapse" id="ui-basic">
            <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="liste_service.php">liste services</a></li>
            <li class="nav-item"> <a class="nav-link" href="en_attente.php">En attente</a></li>
              <li class="nav-item"> <a class="nav-link" href="rejet.php">Rejetté</a></li>
            </ul>
          </div>
        </li>
          <li class="nav-item">
            <a class="nav-link" href="create_category.php">
              <i class="mdi mdi mdi-view-grid menu-icon"></i>
              <span class="menu-title">Secteur</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="create_work.php">
              <i class="mdi mdi  mdi mdi-xing-box menu-icon"></i>
              <span class="menu-title">Profession</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="prestataire.php">
              <i class="mdi mdi mdi-worker menu-icon"></i>
              <span class="menu-title">Prestataires</span>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="users.php">
              <i class="mdi mdi mdi-account-multiple menu-icon"></i>
              <span class="menu-title">Utiliseurs</span>
            </a>
          </li>
          
          
        </ul>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:./partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 px-0 py-0 py-lg-4 d-flex flex-row">
          <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          
            <h4 class="font-weight-bold mb-0 d-none d-md-block mt-1">Espace Administrateur</h4>
            <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item">
                <h4 class="mb-0 font-weight-bold d-none d-xl-block"><?php
                  $date1 = date('Y-m-d'); // Date du jour
                  setlocale(LC_TIME, "fr_FR", "French");
                  echo strftime("%A %d %B %G", strtotime($date1));
                ?></h4>
              </li>
              <li class="nav-item dropdown mr-1">
                <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" title="Message utilisateurs" data-toggle="dropdown">
                  <i class="mdi mdi-email-open mx-0"></i>
                  <span class="count bg-info">2</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                  <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                        <img src="../prestataire/home/images/faces/face4.jpg" alt="image" class="profile-pic">
                    </div>
                    <div class="preview-item-content flex-grow">
                      <h6 class="preview-subject ellipsis font-weight-normal">David Grey
                      </h6>
                      <p class="font-weight-light small-text text-muted mb-0">
                        The meeting is cancelled
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                        <img src="../prestataire/home/images/faces/face2.jpg" alt="image" class="profile-pic">
                    </div>
                    <div class="preview-item-content flex-grow">
                      <h6 class="preview-subject ellipsis font-weight-normal">Tim Cook
                      </h6>
                      <p class="font-weight-light small-text text-muted mb-0">
                        New product launch
                      </p>
                    </div>
                  </a>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                        <img src="../prestataire/home/images/faces/face3.jpg" alt="image" class="profile-pic">
                    </div>
                    <div class="preview-item-content flex-grow">
                      <h6 class="preview-subject ellipsis font-weight-normal"> Johnson
                      </h6>
                      <p class="font-weight-light small-text text-muted mb-0">
                        Upcoming board meeting
                      </p>
                    </div>
                  </a>
                </div>
              </li>
              <li class="nav-item dropdown mr-2">
                <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" title="Notifications administrateur" id="notificationDropdown" href="#" data-toggle="dropdown">
                  <i class="mdi mdi-calendar mx-0"></i>
                  <span class="count bg-danger">1</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                  <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                  <a class="dropdown-item preview-item">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-success">
                        <i class="mdi mdi-information mx-0"></i>
                      </div>
                    </div>
                    <div onclick=notification() class="preview-item-content">
                      <h6 class="preview-subject font-weight-normal">Administrateur</h6>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        10-06-2021
                      </p>
                    </div>
                  </a>
                </div>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
          <div class="navbar-menu-wrapper navbar-search-wrapper d-none d-lg-flex align-items-center">
            <ul class="navbar-nav mr-lg-2">
              <li class="nav-item nav-search d-none d-lg-block">
                <div class="input-group">
                  <input type="text" class="form-control" placeholder="rechercher ici ...." aria-label="search" aria-describedby="search">
                </div>
              </li>
            </ul>
            <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item nav-profile dropdown">
                <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                  <img src="../assets/img/profil/3.png" alt="profile"/>
                  <span class="nav-profile-name">Admin</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">
                  <a class="dropdown-item" href="account.php">
                    <i class="mdi mdi-settings text-primary"></i>
                    Compte
                  </a>
                  <a class="dropdown-item" href="logout.php">
                    <i class="mdi mdi-logout text-primary"></i>
                    Deconnexion
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </nav>
        <div class="main-panel">
          <div class="content-wrapper">
            
          
          <div class="row" style="padding: 25px;">
            <div class="table-responsive">
          <table id="example" class="table table-bordered" style="width:100%">
                  <thead>
                      <tr>
                          <th style="font-weight: bold;">Secteur</th>
                          <th style="font-weight: bold;">Profession</th>
                          <th style="font-weight: bold;">Année Exp</th>
                          <th style="font-weight: bold;">Just profession</th>
                          <th style="font-weight: bold;">Img réalisation</th>
                          <th style="font-weight: bold;">Description</th>
                         <th style="font-weight: bold;">Action</th>

                      </tr>
                  </thead>
                  <tbody>
                  <?php
                      $sql = "SELECT * FROM services WHERE status=-1";
                      $result = $conn->query($sql);
                      if ($result->rowCount() > 0) {
                        $i=1;
                        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                          $id=$row["id"];
                          $img_just=$row["file_just"];
                          $categorie=$row["categorie"];
                          $metier=$row["metier"];
                          $description=$row["description"];
                          ?>
                          <tr>
                          <td><?php echo $i++ ?></td>
                          <td><?php echo $nom_categorie ?></td>
                          <td><?php echo $creation; ?></td>
                           <td></td>
                          <td></td>
                            </tr>
                          <?php
                        }
                      }
                      else {
                      }
                  ?>
                    
              </table>
          </div>
        </div>   
      </div>
    </div>
   <script>
$(document).ready(function() {
    $('#example').DataTable({
      
      "language": {
                "sEmptyTable":     "Aucune donnée disponible dans le tableau",
                "sInfo":           "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
                "sInfoEmpty":      "Affichage de l'élément 0 à 0 sur 0 élément",
                "sInfoFiltered":   "(filtré à partir de _MAX_ éléments au total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Afficher _MENU_ éléments",
                "sLoadingRecords": "Chargement...",
                "sProcessing":     "Traitement...",
                "sSearch":         "Rechercher :",
                "sZeroRecords":    "Aucun élément correspondant trouvé",
                "oPaginate": {
                    "sFirst":    "Premier",
                    "sLast":     "Dernier",
                    "sNext":     "Suivant",
                    "sPrevious": "Précédent"
                },
                "oAria": {
                    "sSortAscending":  ": activer pour trier la colonne par ordre croissant",
                    "sSortDescending": ": activer pour trier la colonne par ordre décroissant"
                },
                "select": {
                    "rows": {
                        "_": "%d lignes sélectionnées",
                        "0": "Aucune ligne sélectionnée",
                        "1": "1 ligne sélectionnée"
                    }
                }
            },
            
    });
} );
  </script>

  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <scr src="../prestataire/home/js/off-canvas.js"></scr>
  <script src="../prestataire/home/js/hoverable-collapse.js"></script>
  <script src="../prestataire/home/js/template.js"></script>
  <!-- endinject -->
  <!-- plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- Custom js for this page-->
  <script src="../prestataire/home/js/dashboard.js"></script>
<script>
function notification()
{
 window.location.assign('notification.php')
 return false;
}
</script></body>

</html>