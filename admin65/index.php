<?php require_once '../controller/adminLogController.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Onsebat | Admin</title>
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css">
	<link rel="icon" type="image/png" href="../assets/img/first/o.png" />

	<style>
		/*custom font*/
		@import url(https://fonts.googleapis.com/css?family=Montserrat);

		/*basic reset*/
		* {
			margin: 0;
			padding: 0;
		}


		body {
			background: #f6f5f7;
			padding-top: 100px;
			font-family: montserrat, arial, verdana;
		}

		/*form styles*/
		#msform {
			width: 600px;
			height: 500px;
			text-align: center;
			position: relative;
		}

		#msform fieldset {
			background: white;
			border: 0 none;
			border-radius: 3px;
			box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
			padding: 20px 30px;
			box-sizing: border-box;
			width: 80%;
			margin: 0 10%;

			/*stacking fieldsets above each other*/
			position: relative;
		}

		/*Hide all except first fieldset*/
		#msform fieldset:not(:first-of-type) {
			display: none;
		}

		/*inputs*/
		#msform input,
		#msform textarea {
			padding: 15px;
			border: 1px solid #ccc;
			border-radius: 3px;
			margin-bottom: 10px;
			width: 100%;
			box-sizing: border-box;
			font-family: montserrat;
			color: #2C3E50;
			font-size: 13px;
		}

		/*buttons*/
		#msform .action-button {
			width: 100px;
			background: #7fad39;
			font-weight: bold;
			color: white;
			border: 0 none;
			border-radius: 1px;
			cursor: pointer;
			padding: 10px 5px;
			margin: 10px 5px;
		}

		#msform .action-button:hover,
		#msform .action-button:focus {
			box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
		}

		/*headings*/
		.fs-title {
			font-size: 15px;
			text-transform: uppercase;
			color: #2C3E50;
			margin-bottom: 10px;
		}

		.fs-subtitle {
			font-weight: normal;
			font-size: 13px;
			color: #666;
			margin-bottom: 20px;
		}
	</style>
</head>

<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3" style="padding-top:120px;">
				<img src="../assets/img/first/logo.png"><br><br>
				<h2 style="font-family: 'Montserrat', sans-serif;font-size:20px;font-weight:bold">ADMIN | CONNEXION </h2>
			</div>
			<div class="col-md-4">
				<form id="msform" method="post" action="index.php" style="padding-top:35px">
					<fieldset>
						<h2 class="fs-title">CONNEXION</h2><br>
						<?php
							if ($err=='') {
							
							}else {
								?><div class="alert alert-danger"><?php echo $err;?></div><?php
							}
						?>
						<input type="email" required name="loginEmail" <?php echo $loginEmail;?> placeholder="Email" />
						<input type="password" required name="loginPass" minlength=7 pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{7,}"  <?php echo $loginPass;?> placeholder="Password" />
						<br><br>
						<input type="submit" name="LoginA" class="next action-button" value="CONNEXION" /><br>

					</fieldset>

				</form>
			</div>
		
		</div>
	</div>

</body>

</html>